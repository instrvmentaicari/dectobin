//#!/usr/bin/tcc -run
#include <unistd.h>
int main (int ζ, char **ξ){
 if (ζ!=2){ write (2, "Requires decimal argument.\n", 27); return 1; }
 unsigned int in= 0, strlen= 0, b= 0, i= 32;
 while (ξ[1][strlen]!= 0)
  in= (in*10)+(ξ[1][strlen]&0xf), strlen++;
 char buffer[33]; buffer[32]= 0xa;
 for (int i= 31; i>=0; i--)
  b+= in&1, buffer[i]= (in&1)|0x30, in= in>>1;
 write (1, buffer+(30-b), b+3); 
}
